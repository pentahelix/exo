package exo;
class C{
	public static var mapScale = 3;
	public static var chunkW = 8;
	public static var chunkH = 8;
	public static var tiles = [
		"empty"             => 0,
		"plain0"            => 1,
		"plain1"            => 2,
		"pit"               => 3,
		"algae"             => 4,

		"rock0_0"           => 5,
		"rock0_1"           => 6,
		"rock0_2"           => 7,
		"rock1_0"           => 8,
		"rock1_1"           => 9,
		"rock1_2"           => 10,

		"ship"              => 11,

		"node_empty"        => 20,
		"node_lock0"        => 21,
		"node_lock1"        => 22,
		"node_crafting"     => 23,
		"node_refining"     => 24,
		"node_oxygen"       => 25,
		"node_building"     => 26,
		"node_teleporter"   => 27,
		"node_farm"         => 28,
		"node_electricity"  => 29,
		"node_grinder"      => 30,
		"node_compressor"   => 31,

		"node_solar"        => 35,

		"connector0"        => 36,
		"connector1"        => 37,
		"connector2"        => 38,
		"connector3"        => 39,
		"connector4"        => 40,

		"visibility0"       => 56,
		"visibility1"       => 57,

		"blue"              => 61,
		"yellow"            => 62,
		"orange"            => 63,
	];
}
