package exo.states;

import luxe.*;
import luxe.States;
import luxe.Text;
import luxe.Input;
import luxe.Rectangle;
import phoenix.Shader;
import phoenix.Batcher;
import phoenix.Texture.FilterType;

class MenuState extends State{
  var planet:Shader;
  var ship:Sprite;
  var offset:Float = 0;
  var nebula0:Shader;
  var nebula1:Shader;
  public static var menuBatch:Batcher;
  override public function onenter<T>(_:T){
    Luxe.renderer.clear_color = new Color(0,0,0,1);
    Render.ui.add_underlay(new Rectangle(100,0,400,Luxe.screen.h));
    Luxe.resources.texture("res/intro_planetTex.png").filter_mag = FilterType.nearest;
    Luxe.resources.texture("res/title.png").filter_mag = FilterType.nearest;
    Luxe.resources.texture("res/intro_ship.png").filter_mag = FilterType.nearest;
    Luxe.resources.texture("res/nebula0.png").filter_mag = FilterType.nearest;
    Luxe.resources.texture("res/nebula1.png").filter_mag = FilterType.nearest;

    Luxe.resources.texture("res/intro_planetTex.png").clamp_t = phoenix.Texture.ClampType.repeat;
    Luxe.resources.texture("res/intro_planetTex.png").clamp_s = phoenix.Texture.ClampType.repeat;

    Luxe.resources.texture("res/nebula0.png").clamp_t = phoenix.Texture.ClampType.repeat;
    Luxe.resources.texture("res/nebula0.png").clamp_s = phoenix.Texture.ClampType.repeat;
    Luxe.resources.texture("res/nebula1.png").clamp_s = phoenix.Texture.ClampType.repeat;
    Luxe.resources.texture("res/nebula1.png").clamp_t = phoenix.Texture.ClampType.repeat;

    menuBatch = new Batcher(Luxe.renderer, "batch");
    menuBatch.view = new phoenix.Camera();
    menuBatch.layer = 2;
    Luxe.renderer.add_batch(menuBatch);

    planet = Luxe.resources.shader("sphere").clone("");
    new Sprite({
      pos: Luxe.screen.mid,
      size: Luxe.screen.size,
      color: new Color(0,0,0,1),
      depth: -3,
    });
    new Sprite({
      texture: Luxe.resources.texture("res/intro_planetTex.png"),
      shader: planet,
      pos: new Vector(Luxe.screen.w-400, Luxe.screen.h+160),
      size: new Vector(64,64),
      scale: new Vector(12,12),
      color: new Color(1,1,1,1),
      depth: 0,
    });

    make_ship();

    new Sprite({
      texture: Luxe.resources.texture("res/title.png"),
      pos: new Vector(300, 120),
      scale: new Vector(10,10),
      depth: 1,
      batcher: menuBatch,
    });

    for(i in 0...100){
      new Star();
    }

    new Sprite({
      pos: new Vector(0,0),
      size: Luxe.screen.size,
      centered: false,
      color: new Color(.51, .81, .88, .4),
    });

    nebula0 = new Sprite({
      texture: Luxe.resources.texture("res/nebula0.png"),
      size: new Vector(Luxe.screen.w, Luxe.screen.w),
      centered: false,
      depth: 5,
      color: new Color(1,1,1,.4),
      shader: Luxe.resources.shader("scroll").clone(""),
    }).shader;

    nebula1 = new Sprite({
      texture: Luxe.resources.texture("res/nebula1.png"),
      size: new Vector(Luxe.screen.w, Luxe.screen.w),
      centered: false,
      depth: 5,
      color: new Color(1,1,1,.4),
      shader: Luxe.resources.shader("scroll").clone(""),
    }).shader;

    new MenuText(300,300,"New Game", function(str){
      Main.state.set("world");
    });
    new MenuText(300,380,"Continue", function(str){trace(str);});
    new MenuText(300,460,"About", function(str){trace(str);});
  }

  override public function onleave<T>(_:T){
    Render.ui.empty();
    menuBatch.destroy(true);
    Luxe.renderer.batcher.empty(false);
  }

  override public function update(dt:Float){
    offset -= dt;
    planet.set_float("offset", offset/8);
    nebula0.set_float("offset", offset/3);
    nebula1.set_float("offset", offset/2);

    ship.pos.y = Luxe.screen.mid.y+Math.sin(offset)*15;
  }

  private function make_ship(){
    ship = new Sprite({
      texture: Luxe.resources.texture("res/intro_ship.png"),
      pos: new Vector(Luxe.screen.w-400, Luxe.screen.mid.y-200),
      scale: new Vector(6,6),
      depth: 2,
    });
    new Sprite({
      pos: new Vector(27,15),
      size: new Vector(100,5),
      color: new Color(0,0,0,.8).rgb(0x2df0ff),
      parent: ship,
      centered: false,
      depth: 3,
    });
    new Sprite({
      pos: new Vector(27,11),
      size: new Vector(100,5),
      color: new Color(0,0,0,.8).rgb(0x2df0ff),
      parent: ship,
      centered: false,
      depth: 1,
    });
    new Sprite({
      pos: new Vector(27,16),
      size: new Vector(1,3),
      color: new Color(.6,.68,.72,1),
      parent: ship,
      centered: false,
      depth: 4,
    });
  }
}

class Star extends Sprite{
  var deltaRot:Float;
  override public function new(){
    super({
      pos: new Vector(Luxe.utils.random.get() * Luxe.screen.w, Luxe.utils.random.get() * Luxe.screen.h),
      rotation_z: Luxe.utils.random.get() * 180,
      depth: -2,
    });
    var sizePx:Int = 3 + Std.int(Luxe.utils.random.get() * 7);
    size = new Vector(sizePx, sizePx);
    deltaRot = Luxe.utils.random.float(-20,20);
  }

  override function update(dt:Float){
    rotation_z += dt * deltaRot;
  }
}

class MenuText extends Text{
  var handler:String->Void;
  override public function new(x:Float,y:Float,txt:String,onclick:String->Void){
    super({
      font: Luxe.resources.font("res/font/font.fnt"),
      text: txt,
      align: TextAlign.center,
      pos: new Vector(x, y),
      color: new Color(.8,.8,.8,1),
      batcher: MenuState.menuBatch,
    });
    handler = onclick;
  }

  override public function onmousedown(e:MouseEvent){
    if(point_inside(e.pos))handler(text);
  }
}
