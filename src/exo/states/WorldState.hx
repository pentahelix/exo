package exo.states;

import luxe.*;
import luxe.Input;
import luxe.States;
import luxe.Events;
import luxe.Screen.WindowEvent;
import luxe.Text.TextAlign;
import luxe.tilemaps.Tilemap.Tile;
import luxe.tween.Actuate;
import phoenix.*;
import phoenix.Batcher.BatcherEventType;
import phoenix.Texture.FilterType;
import exo.*;
import exo.map.*;
import exo.Player.Activity;
import exo.BaseComplex.NodeType;


class WorldState extends State {
	public static var player:Player;
	public static var blurShader:Shader;
	public static var currentBase:BaseComplex;
	var map:Worldmap;
	var base:BaseComplex;
	var inBuilding:Bool = false;
	var baseFocus:Bool = false;
	var baseCursor:BaseCursor;

	var hxScout:Bool = false;
	var hxt:hxtelemetry.HxTelemetry;

	override function onenter<T>(_:T){
		Luxe.resources.texture("res/mineral_blue.png").filter_mag = FilterType.nearest;
		Luxe.resources.texture("res/mineral_red.png").filter_mag = FilterType.nearest;
		Luxe.resources.texture("res/oxygen.png").filter_mag = FilterType.nearest;

		player = new Player();
		player.gridPos = new Vector(2,3);
		player.update_position();
		Luxe.camera.focus(player.pos, 0);

		map = new Worldmap();

		base = new BaseComplex(new Vector(3,3));
		base.addNode(new Vector(2,3), NodeType.AIRLOCK_CLOSED);
		base.addNode(new Vector(3,2), NodeType.CRAFTING);
		map.add_base(base);
		currentBase = base;
		map.update_base(base);
		toggle_inBuilding();

		baseCursor = new BaseCursor();

		Luxe.camera.focus(player.pos, 0);

		setup_ui();

		//Render Specific
		//DONE:20 Make enabling/disabling hxScout easier
		if(hxScout){
			var cfg = new hxtelemetry.HxTelemetry.Config();
	    cfg.allocations = true;
	    cfg.app_name = "Luxe";
	    hxt = new hxtelemetry.HxTelemetry(cfg);
		}
	}

	override public function onleave<T>(_:T){
		Render.ui.empty();
	}

	override function onmousemove(e:MouseEvent){
		if(player.activity == Activity.BUILDING){

		}
	}

	override function update(dt:Float){
		if(hxScout)hxt.advance_frame();
	}

	private function toggle_base_building(){
		player.activity = player.activity == Activity.WALKING ? Activity.BUILDING : Activity.WALKING;
		if(player.activity == Activity.BUILDING){
			baseCursor.show(true);
			currentBase.focus_camera(2);
		}else{
			baseCursor.show(false);
			currentBase.focus_camera(0);
		}
	}

	private function setup_ui(){
    Render.ui.add_icon(new Vector(180,30), "res/mineral_blue.png");
    Render.ui.add_icon(new Vector(330,30), "res/mineral_red.png");

    Render.ui.add_text(new Vector(60,8), "0");
    Render.ui.add_text(new Vector(210,8), "0");
    Render.ui.add_text(new Vector(360,8), "0");

    Render.ui.add_underlay(new Rectangle(0,0,450,60));
		new UI.UIBar(new Rectangle(10,10,100,20), new Color().rgb(0xff4433), Render.ui, 0, 100, 75);
	}

	private function toggle_inBuilding(){
		inBuilding = !inBuilding;
		if(inBuilding){
			player.oxygen = player.oxytankSize;
			currentBase = map.get_base_at(player.gridPos);
			currentBase.updateNode(player.gridPos, NodeType.AIRLOCK_CLOSED);
		}else{
			currentBase.updateNode(player.gridPos, NodeType.AIRLOCK);
			currentBase = null;
		}
		toggle_base_focus();
	}

	private function toggle_crafting(){
		player.activity = player.activity == Activity.WALKING ? Activity.CRAFTING : Activity.WALKING;
		if(player.activity == Activity.CRAFTING){
			//Actuate.tween(UI.resourcesUnderlay.size , .2, {y: 120}).ease(luxe.tween.easing.Quad.easeIn);
		}else{
			//Actuate.tween(UI.resourcesUnderlay.size , .2, {y: 60}).ease(luxe.tween.easing.Quad.easeIn);
		}
	}

	private function toggle_base_focus(){
		baseFocus = !baseFocus;
		if(baseFocus){
			player.canMove = true;
			currentBase.focus_camera();
		}else{
			Actuate.tween(Luxe.camera, .4, {zoom: 2}).ease(luxe.tween.easing.Quad.easeIn);
		}
	}

	// INPUT
	override public function oninputdown(event_name:String, event:InputEvent){
		ChunkLoader.update_chunks(player.gridPos);
		var x:Int = Std.int(player.gridPos.x);
		var y:Int = Std.int(player.gridPos.y);

		if(event_name == "interact"){
			var t:Tile = ChunkLoader.tile_at_layer("buildings", x, y);
			var id:Int = map.get_tile_at(t.x,t.y);
			if(id == C.tiles["node_lock0"] || id == C.tiles["node_lock1"]){
				toggle_inBuilding();
			}else if(id == C.tiles["node_building"]){
				toggle_base_building();
			}else if(id == C.tiles["node_crafting"]){
				toggle_crafting();
			}
		}

		if(player.activity != Activity.WALKING || !player.canMove)return;
		var oldpos:Vector = new Vector(Std.int(player.gridPos.x), Std.int(player.gridPos.y));
		switch(event_name){
		case "move_up":
			player.gridPos.y--;
			player.anim.animation = "up_idle";
			player.flipx = false;
		case "move_down":
			player.gridPos.y++;
			player.anim.animation = "down_idle";
			player.flipx = false;
		case "move_left":
			player.gridPos.x--;
			player.anim.animation = "side_idle";
			player.flipx = false;
		case "move_right":
			player.gridPos.x++;
			player.anim.animation = "side_idle";
			player.flipx = true;
		}
		x = Std.int(player.gridPos.x);
		y = Std.int(player.gridPos.y);

		var colId:Int = ChunkLoader.tile_at_layer("collision", x, y).id;
		var nodeId:Int = ChunkLoader.get_tile_at(x,y);
		var validMove:Bool = false;

		if(inBuilding){
			player.oxygen = player.oxytankSize;
			if(colId == C.tiles["orange"]){
				validMove = true;
			}
			if(nodeId == C.tiles["node_lock1"]){
				validMove = true;
			}
			if(nodeId == C.tiles["node_lock0"]){
				map.interact_at(x,y);
				validMove = false;
			}
		}else{
			if(colId == C.tiles["empty"] || nodeId == C.tiles["node_lock0"]){
				validMove = true;
			}else if(colId == C.tiles["yellow"] || colId == C.tiles["blue"]){
				map.interact_at(x,y);
			}
		}

		if(validMove){
			player.update_position();
			player.oxygen--;
			map.reveal_at(player.gridPos);
			player.canMove = false;
			Luxe.camera.focus(player.pos, .095, function(){player.canMove = true;});
		}else{
			player.gridPos = oldpos;
		}
	}
	override function onmousedown(event:MouseEvent){
		// UI.barHealth.value -= 1;
		if(player.activity == Activity.BUILDING){
			var worldPos:Vector = Luxe.camera.screen_point_to_world(event.pos);
			var c:Chunk = ChunkLoader.chunk_at_point(worldPos);
			var t:Tile = c.tile_at_pos("collision", worldPos.x, worldPos.y, C.mapScale);
			if(t.id == C.tiles["empty"]){
				currentBase.placeNode(new Vector(c.chunkPos.x*C.chunkW+t.x, c.chunkPos.y*C.chunkH+t.y), baseCursor.get_NodeType());
				map.update_base(currentBase);
				currentBase.focus_camera(2.5);
			}
		}
	}
}
