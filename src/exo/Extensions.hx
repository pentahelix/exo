package exo;
import luxe.Vector;
import exo.map.Chunk;

class ArrayExt{
  public static function contains(a:Array<Vector>, v:Vector):Bool{
    for(vec in a){
      if(vec.equals(v))return true;
    }
    return false;
  }

  public static function drop(a:Array<Vector>, v:Vector):Void{
    for(i in 0...a.length){
      if(a[i].equals(v)){
        a.splice(i,1);
        return;
      }
    }
  }
}

class IntExt{
  public static function mod(f1:Int, f2:Int):Int{
    var r = f1 % f2;
    if(r < 0) r+=f2;
    return r;
  }
}

class VectorExt{
  public static function to_string(v:Vector):String{
    return v.x+","+v.y;
  }

  public static function round_to(v:Vector, i:Int):Vector{
    return new Vector(Math.floor(v.x/i)*i,Math.floor(v.y/i)*i);
  }
}

class StringExt{
  public static function to_vector(s:String):Vector{
    return new Vector(Std.parseFloat(s.split(",")[0]), Std.parseFloat(s.split(",")[1]));
  }
}
