package exo;

import luxe.Sprite;
import luxe.Vector;
import luxe.Color;
import luxe.Text;
import exo.states.WorldState;
import phoenix.geometry.Geometry;
import phoenix.geometry.Vertex;
import phoenix.geometry.TextGeometry;
import phoenix.Texture;
import luxe.Text.TextAlign;
import luxe.Rectangle;
import phoenix.Shader;
import phoenix.Batcher;
import phoenix.Batcher.BatcherEventType;

class UI extends Batcher{
  var shaders:Array<Shader> = [];
  var renderTex:Texture;

  override public function new(tex:Texture){
    super(Luxe.renderer, "ui_batcher");
    view = new phoenix.Camera();
    layer = 2;

    renderTex = tex;
    this.on(BatcherEventType.prerender, update_ui);
  }

  public function update_ui(b:Batcher){
    //TODO: Rework update_ui
    for(shader in shaders){
      shader.set_texture("tex0", renderTex);
    }
  }

  public function add_icon(pos:Vector, id:String){
    new Sprite({
      texture: Luxe.resources.texture(id),
      pos: pos,
      scale: new Vector(3,3),
      batcher: this,
      depth: 2
    });
  }

  public function add_underlay(rect:Rectangle):UIUnderlay{
    var shader:Shader = Luxe.resources.shader("blurBelow").clone("");
		shader.set_vector2("uShift", new Vector(1/Luxe.screen.w, 1/Luxe.screen.h));
    shader.set_vector4("transform", new Vector(rect.x/Luxe.screen.w, rect.y/Luxe.screen.h, rect.w/Luxe.screen.w, rect.h/Luxe.screen.h));
    shaders.push(shader);
    return new UIUnderlay(rect, this, shader);
  }

  public function add_text(pos:Vector, text:String):TextGeometry{
    return Luxe.draw.text({
      color: new Color(1,1,1,1),
      pos: pos,
      point_size: 48,
      align: TextAlign.left,
      text: text,
      batcher: this,
      font: Luxe.resources.font("res/font/font.fnt"),
    });
  }
}

class UIUnderlay extends Sprite{
  var uiShader:Shader;
  var prevPos:Vector;
  var prevSize:Vector;
  override public function new(rect:Rectangle, batch:Batcher, s:Shader){
    super({
      size: new Vector(rect.w,rect.h),
      centered: false,
      pos: new Vector(rect.x,rect.y),
      shader: s,
      batcher: batch,
      color: new Color(.8,.8,.8,1),
      depth: -1
    });
    uiShader = s;
    prevPos = pos.clone();
    prevSize = size.clone();
  }

  override public function update(dt:Float){
    if(!pos.equals(prevPos) || !size.equals(prevSize)){
      uiShader.set_vector4("transform", new Vector(pos.x/Luxe.screen.w, pos.y/Luxe.screen.h, size.x/Luxe.screen.w, size.y/Luxe.screen.h));
      prevPos = pos.clone();
      prevSize = size.clone();
    }
  }
}

class UIBar extends Sprite{
  var min:Float;
  var max:Float;
  var c:Color;
  var r:Rectangle;

  public var value(default,set):Float;

  public function set_value(v:Float){
    value = Math.max(v,min);
    var barW:Float = (value-min)/(max-min)*r.w;
    geometry.vertices[2].pos = new Vector(barW,r.h);
    geometry.vertices[4].pos = new Vector(barW,0);
    geometry.vertices[5].pos = new Vector(barW,r.h);

    geometry.vertices[6].pos = new Vector(barW,0);
    geometry.vertices[7].pos = new Vector(barW,r.h);
    geometry.vertices[9].pos = new Vector(barW,0);
    return value;
  }

  override public function new(rect:Rectangle, color:Color, batch:Batcher, minV:Float, maxV:Float, ?cur:Float){
    super({
      pos: new Vector(rect.x, rect.y),
      size: new Vector(rect.w, rect.h),
      centered: false,
      batcher: batch,
    });

    set_geometry(new Geometry({
      batcher: batch,
      primitive_type: PrimitiveType.triangles,
    }));

    c = color;
    max= maxV;
    min = minV;
    r = rect;

    var barW:Float = ((cur!=null ? cur : max)-min)/(max-min)*rect.w;
    var secC:Color = new Color(c.r*.65, c.g*.65,c.b*.65, 1);

    geometry.add(new Vertex(new Vector(0,0), c));
    geometry.add(new Vertex(new Vector(0,r.h), c));
    geometry.add(new Vertex(new Vector(barW,r.h), c));

    geometry.add(new Vertex(new Vector(0,0), c));
    geometry.add(new Vertex(new Vector(barW,0), c));
    geometry.add(new Vertex(new Vector(barW,r.h), c));

    geometry.add(new Vertex(new Vector(barW,0), secC));
    geometry.add(new Vertex(new Vector(barW,r.h), secC));
    geometry.add(new Vertex(new Vector(r.w,r.h), secC));

    geometry.add(new Vertex(new Vector(barW,0), secC));
    geometry.add(new Vertex(new Vector(r.w,0), secC));
    geometry.add(new Vertex(new Vector(r.w,r.h), secC));

    if(cur != null)set_value(cur);
    else set_value(max);
  }
}
