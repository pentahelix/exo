package exo;

import luxe.Sprite;
import luxe.Events;
import luxe.Input;
import luxe.Color;
import luxe.Vector;
import luxe.Rectangle;
import exo.states.WorldState;
import phoenix.Texture;
import phoenix.Texture.FilterType;
import exo.BaseComplex.NodeType;

using Extensions.VectorExt;
using Extensions.IntExt;

class BaseCursor extends Sprite{
  var gridPos:Vector;
  var selectedNode:Int = 0;
  var buildableNodes:Array<NodeType> = [
    NodeType.EMPTY,
    NodeType.HUB,
    NodeType.AIRLOCK_CLOSED,
    NodeType.CRAFTING,
    NodeType.REFINING,
    NodeType.OXYGEN,
    NodeType.TELEPORTER,
    NodeType.FARM,
    NodeType.ELECTRICITY,
    NodeType.SOLAR,
  ];

  override public function new(){
    var tex:Texture = Luxe.resources.texture("res/base_cursor.png");
		tex.filter_min =tex.filter_mag = FilterType.nearest;
		super({
			name: "player",
      shader: Luxe.resources.shader("tint"),
			scale: new Vector(3,3),
			size: new Vector(16,16),
			pos: new Vector(0,0),
			depth: .3,
      color: new Color().rgb(0x11DD11),
      texture: tex,
		});
    gridPos = new Vector(0,0);
    update_geom();
    show(false);
  }

  public function show(b:Bool){
    active = b;
    visible = b;
  }

  override public function onmousemove(e:MouseEvent){
    var newPos:Vector = Luxe.camera.screen_point_to_world(e.pos).round_to(16 * C.mapScale).divideScalar(16 * C.mapScale);
    if(gridPos.equals(newPos))return;
    gridPos = newPos;
    pos = Luxe.camera.screen_point_to_world(e.pos)
          .round_to(16 * C.mapScale)
          .add(new Vector(16 * C.mapScale / 2, 16 * C.mapScale / 2));
    check_buildable();
  }

  override function onmousewheel(e:MouseEvent){
    selectedNode = (selectedNode-e.yrel).mod(buildableNodes.length);
    update_geom();
    check_buildable();
  }

  public function get_NodeType():NodeType{
    return buildableNodes[selectedNode];
  }

  private function update_geom(){
    this.set_uv(new Rectangle(0,selectedNode * 16,16,16));
  }

  private function check_buildable(){
    if(WorldState.currentBase.can_build_at(gridPos, buildableNodes[selectedNode])){
      color = new Color().rgb(0x17bc0f);
    }else{
      color = new Color().rgb(0xbc0f16);
    }
  }
}
