package exo.map;

import exo.BaseComplex.BaseNode;
import exo.BaseComplex.NodeType;
import luxe.tilemaps.Tilemap;
import luxe.utils.Random;
import luxe.Vector;
import exo.states.WorldState;
import noisehx.Perlin;
import phoenix.Texture;
import phoenix.Texture.ClampType;
import phoenix.Texture.FilterType;
using Extensions.VectorExt;

// COLLISION LAYER
// 63 .......... Building
// 62 .......... Minables
// 61 .......... Airlock

class Worldmap{
	public static var seed:Int = 141;
	public static var bases:Array<BaseComplex> = [];
	public static var resources:Map<String, Int> = new Map<String, Int>();
	public static var visibility:Map<String, Int> = new Map<String, Int>();
	public static var baseNodeMap:Map<String, BaseNode> = new Map<String, BaseNode>();
	public static var baseMap:Map<String, BaseComplex> = new Map<String, BaseComplex>();
	public var random:Random;


	public static var nodeId:Map<NodeType, Int> = [
		NodeType.EMPTY =>          C.tiles["node_empty"],
		NodeType.AIRLOCK =>        C.tiles["node_lock0"],
		NodeType.CRAFTING =>       C.tiles["node_crafting"],
		NodeType.REFINING =>       C.tiles["node_refining"],
		NodeType.OXYGEN =>         C.tiles["node_oxygen"],
		NodeType.HUB =>            C.tiles["node_building"],
		NodeType.TELEPORTER =>     C.tiles["node_teleporter"],
		NodeType.AIRLOCK_CLOSED => C.tiles["node_lock1"],
		NodeType.FARM =>           C.tiles["node_farm"],
		NodeType.ELECTRICITY =>    C.tiles["node_electricity"],
		NodeType.SOLAR =>          C.tiles["node_solar"],
	];

	public function new(){
		ChunkLoader.update_chunks(WorldState.player.gridPos);
	}

	public inline function get_tile_at(x:Int, y:Int){
	 	return ChunkLoader.get_tile_at(x,y);
	}

	public function interact_at(x:Int, y:Int){
		var k:String = new Vector(x,y).to_string();
		WorldState.player.oxygen--;
		if(baseNodeMap[k] != null){
			if(baseNodeMap[k].type == NodeType.AIRLOCK_CLOSED){
				baseNodeMap[k].type = NodeType.AIRLOCK;
				ChunkLoader.chunk_at(new Vector(x,y)).update_node(baseNodeMap[k]);
			}else if(baseNodeMap[k].type == NodeType.AIRLOCK){
				baseNodeMap[k].type = NodeType.AIRLOCK_CLOSED;
				ChunkLoader.chunk_at(new Vector(x,y)).update_node(baseNodeMap[k]);
			}
		}
		if(resources[k] > 0){
			var id:Int = ChunkLoader.tile_at_layer("ground", x, y).id;
			if(id == C.tiles["rock0_0"] || id == C.tiles["rock0_1"] || id == C.tiles["rock0_2"]){
				WorldState.player.mineralsBlue++;
				resources[k]-=1;
				switch(resources[k]){
					case 10:
						ChunkLoader.tile_at_layer("ground", x, y).id = C.tiles["rock0_1"];
					case 5:
						ChunkLoader.tile_at_layer("ground", x, y).id = C.tiles["rock0_2"];
					case 0:
						ChunkLoader.tile_at_layer("ground", x, y).id = C.tiles["plain0"];
						ChunkLoader.tile_at_layer("collision", x, y).id = C.tiles["empty"];
				}
			}
			if(id == C.tiles["rock1_0"] || id == C.tiles["rock1_1"] || id == C.tiles["rock1_2"]){
				WorldState.player.mineralsRed++;
				resources[k]-=1;
				switch(resources[k]){
					case 6:
						ChunkLoader.tile_at_layer("ground", x, y).id = C.tiles["rock1_1"];
					case 2:
						ChunkLoader.tile_at_layer("ground", x, y).id = C.tiles["rock1_2"];
					case 0:
						ChunkLoader.tile_at_layer("ground", x, y).id = C.tiles["plain0"];
						ChunkLoader.tile_at_layer("collision", x, y).id = C.tiles["empty"];
				}
			}
		}
	}
	//TODO: Is this needed?
	public function add_base(b:BaseComplex){
		bases.push(b);
		for(n in b.nodes){
			baseNodeMap[n.pos.to_string()] = n;
			if(n.type == NodeType.AIRLOCK || n.type == NodeType.AIRLOCK_CLOSED){
				baseMap[n.pos.to_string()] = b;
			}
		}
	}

	public function reveal_at(p:Vector){
		set_visibility(p, 2);
		set_visibility(new Vector(p.x, p.y-1), 2);
		set_visibility(new Vector(p.x, p.y+1), 2);
		set_visibility(new Vector(p.x-1, p.y), 2);
		set_visibility(new Vector(p.x+1, p.y), 2);

		set_visibility(new Vector(p.x-1, p.y-1), 1);
		set_visibility(new Vector(p.x-1, p.y+1), 1);
		set_visibility(new Vector(p.x+1, p.y-1), 1);
		set_visibility(new Vector(p.x+1, p.y+1), 1);
	}

	private function set_visibility(p:Vector, v:Int){
		visibility.set(p.to_string(), Std.int(Math.max(v, visibility.get(p.to_string()))));
		ChunkLoader.tile_at_layer("visibility", Math.floor(p.x), Math.floor(p.y)).id = 56 + visibility.get(p.to_string());
	}

	public function get_base_at(p:Vector){
		return baseMap[p.to_string()];
	}

	public function update_base(b:BaseComplex){
		for(n in b.nodes){
			ChunkLoader.set_node_at(n.pos, n);
			baseNodeMap[n.pos.to_string()] = n;
		}
	}

	public static function set_resource(x:Int,y:Int,r:Int){
		var k:String = new Vector(x, y).to_string();
		if(resources[k] == null){
			resources[k] = r;
			return true;
		}else if(resources[k] == 0){
			return false;
		}else{
			return true;
		}
	}
}
