package exo.map;

import luxe.Vector;
import luxe.tilemaps.Tilemap.Tile;
import exo.map.Chunk;
import exo.BaseComplex.BaseNode;
import exo.BaseComplex.NodeType;
import phoenix.Texture.FilterType;
import cpp.vm.Thread;
using Extensions.ArrayExt;
using Extensions.StringExt;
using Extensions.VectorExt;
using Extensions.IntExt;


class ChunkLoader{
  //DONE:10 Change Map keys from Vector to String
  public static var loadedChunks:Map<String, Chunk> = new Map<String, Chunk>();
  public static var displayedChunks:Array<String> = [];

  public static function get_tile_at(x:Int, y:Int){
    var vec:Vector = new Vector(Math.floor(x/C.chunkW), Math.floor(y/C.chunkH));
    x = x.mod(C.chunkW);
    y = y.mod(C.chunkH);
    var c = loadedChunks[vec.to_string()];
    if(c == null){
      trace("Chunk not loaded");
      return 0;
    }
    return c.tile_at("buildings", x, y).id == 0 ? c.tile_at("ground", x, y).id : c.tile_at("buildings", x, y).id;
  }
  public static function tile_at_layer(str:String, x:Int, y:Int){
    var vec:Vector = new Vector(Math.floor(x/C.chunkW), Math.floor(y/C.chunkH));
    var c = loadedChunks[vec.to_string()];
    if(c == null)trace("Chunk not loaded");
    return c.tile_at(str, x.mod(C.chunkW), y.mod(C.chunkH));
  }
  public static function tile_at_point(layer:String, pnt:Vector){
    for(c in loadedChunks){
      var tile:Tile = c.tile_at_pos(layer,pnt.x, pnt.y,C.mapScale);
      if(tile != null)return tile;
    }
    return null;
  }
  public static function chunk_at_point(pnt:Vector){
    for(c in loadedChunks){
      var tile:Tile = c.tile_at_pos("collision",pnt.x, pnt.y,C.mapScale);
      if(c.tile_at_pos("collision", pnt.x, pnt.y,C.mapScale) != null)return c;
    }
    return null;
  }
  public static function reload_chunks(pos:Vector){
    var keys:Iterator<String> = loadedChunks.keys();
    for(k in keys){
      loadedChunks[k].destroy();
      loadedChunks.remove(k);
    }
    update_chunks(pos);
  }

  //TODO:10 Load Chunks async
  public static function update_chunks(pos:Vector, ?threading:Bool=true){
    threading=false;
    if(threading){
      var t:Thread = Thread.create(_update_chunks_thread);
      t.sendMessage(pos);
    }else{
      _update_chunks(pos);
    }
  }

  private static function _update_chunks_thread(){
    _update_chunks(Thread.readMessage(true));
  }

  private static function _update_chunks(pos:Vector=null){
    var w:Int = C.chunkW;
    var h:Int = C.chunkH;
    var x:Int = Math.floor(pos.x/w);
    var y:Int = Math.floor(pos.y/h);

    var requieredChunks:Array<String> = [
      new Vector(x,   y).to_string(),

      new Vector(x+1, y).to_string(),
      new Vector(x,   y+1).to_string(),
      new Vector(x-1, y).to_string(),
      new Vector(x,   y-1).to_string(),

      new Vector(x+1, y+1).to_string(),
      new Vector(x-1, y-1).to_string(),
      new Vector(x+1, y-1).to_string(),
      new Vector(x-1, y+1).to_string(),
    ];
    var keys:Iterator<String> = loadedChunks.keys();
    for(k in keys){
      if(requieredChunks.indexOf(k) != -1){
        requieredChunks.remove(k);
      }else{
        //TODO:0 Cache Chunks (Only hide and don't destroy them)
        loadedChunks[k].destroy();
        loadedChunks.remove(k);
      }
    }

    for(v in requieredChunks){
      var vec:Vector = v.to_vector();
      loadedChunks.set(v, new Chunk(Std.int(vec.x), Std.int(vec.y)));
      //trace('New Chunk loaded at $v');
    }
  }
  public static function chunk_at(v:Vector):Chunk{
    var c:Chunk = loadedChunks[new Vector(Math.floor(v.x/C.chunkW), Math.floor(v.y/C.chunkH)).to_string()];
    if(c == null){
      trace("Chunk not loaded");
    }
    return c;
  }
  public static function set_node_at(v:Vector, n:BaseNode){
    chunk_at(v).update_node(n);
    if(n.type == NodeType.AIRLOCK || n.type == NodeType.AIRLOCK_CLOSED){
      Worldmap.baseMap.set(n.pos.to_string(), n.base);
    }
  }

  private static function show_chunk(c:Chunk, b:Bool){
    for(layer in c.visual.geometry){
      for(row in layer){
        for(tile in row){
          if(tile != null)tile.visible = b;
        }
      }
    }
  }
}
