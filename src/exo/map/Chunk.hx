package exo.map;

import exo.BaseComplex.BaseNode;
import exo.BaseComplex.NodeType;
import luxe.Vector;
import luxe.tilemaps.Tilemap;
import noisehx.Perlin;
import phoenix.Texture;
import phoenix.Texture.ClampType;
import phoenix.Texture.FilterType;

using Extensions.VectorExt;
using Extensions.IntExt;

class Chunk extends Tilemap{
  public var chunkPos:Vector;
  public function new(mX:Int, mY:Int){
    super({
			x: mX * 16 * C.chunkW * C.mapScale,
			y: mY * 16 * C.chunkH * C.mapScale,
			w: C.chunkW,
			h: C.chunkH,
			tile_width: 16,
			tile_height: 16,
			orientation: TilemapOrientation.ortho,
		});

    chunkPos = new Vector(mX,mY);

    setup_layers();
    load_tiles();
    Luxe.utils.random.initial = Worldmap.seed;

    display({
      filter: FilterType.nearest,
      scale: C.mapScale,
      depth: 0
    });
  }
  public function update_node(node:BaseNode){
    if(node == null)return;
		var tx:Int = Std.int(node.pos.x).mod(8);
		var ty:Int = Std.int(node.pos.y).mod(8);
		tile_at("buildings", tx, ty).id = Worldmap.nodeId[node.type];
		if(node.type == NodeType.AIRLOCK || node.type == NodeType.AIRLOCK_CLOSED){
			tile_at("collision",tx,ty).id = C.tiles["blue"];
    }else{
			tile_at("collision",tx,ty).id = C.tiles["orange"];
		}
    //TODO:20 Optimize this to only update Tiles adjacent to this
    for(n in node.base.nodes){
      set_connections(n);
    }
  }
  private function setup_layers(){
    var tex:Texture = Luxe.resources.texture("res/tiles.png");
    tex.clamp_s = tex.clamp_t = ClampType.repeat;
    tex.filter_mag = tex.filter_min = FilterType.nearest;
    add_tileset({
      tile_width: 16,
      tile_height: 16,
      texture: tex,
      name: "default",
      first_id: 0,
    });

    add_layer({
      name: "ground",
      visible: true,
      opacity: 1,
      layer: 1,
    });
    add_layer({
      name: "buildings",
      visible: true,
      opacity: 1,
      layer: 2,
    });
    add_layer({
      name: "connections",
      visible: true,
      opacity: 1,
      layer: 3,
    });
    add_layer({
      name: "collision",
      visible: false,
      opacity: .1,
      layer: 4,
    });
    add_layer({
      name: "visibility",
      visible: false,
      opacity: .7,
      layer: 5,
    });

    add_tiles_fill_by_id("ground",C.tiles["plain0"]);
    add_tiles_fill_by_id("buildings",C.tiles["empty"]);
    add_tiles_fill_by_id("connections",C.tiles["empty"]);
    add_tiles_fill_by_id("collision",C.tiles["empty"]);
    add_tiles_fill_by_id("visibility",C.tiles["visibility0"]);
  }
  private function load_tiles(){
    //DONE:0 Load base Nodes
    var p:Perlin = new Perlin(Worldmap.seed);
    var lX:Int = 0;
    var lY:Int = 0;
    for(x in Std.int(chunkPos.x*C.chunkW)...Std.int((chunkPos.x*C.chunkW)+C.chunkW)){
      lY = 0;
			for(y in Std.int(chunkPos.y*C.chunkH)...Std.int((chunkPos.y*C.chunkH)+C.chunkH)){
				if(p.noise2d(x/75,y/75, 4) > .8){
					if(Luxe.utils.random.bool(.05)){
						tile_at("ground", lX,lY).id = C.tiles["plain1"];
					}else{
						if(Luxe.utils.random.bool(.7)){
							if(Worldmap.set_resource(x,y,15)){
                tile_at("ground", lX,lY).id = C.tiles["rock0_0"];
                tile_at("collision", lX,lY).id = C.tiles["yellow"];
              }
						}else{
							if(Worldmap.set_resource(x,y,10)){
                tile_at("ground", lX,lY).id = 8;
                tile_at("collision", lX,lY).id = C.tiles["yellow"];
              }
						}
          }
				}else{
					if(Luxe.utils.random.bool(.1)){
						tile_at("ground", lX,lY).id = 2;
					}
				}
        update_node(Worldmap.baseNodeMap[new Vector(x, y).to_string()]);
        lY++;
			}
      lX++;
    }
  }
  private function on_chunk(pos:Vector){
    if(pos.x < chunkPos.x*C.chunkW)     return false;
    if(pos.x >= (chunkPos.x+1)*C.chunkW)return false;
    if(pos.y < chunkPos.y*C.chunkH)     return false;
    if(pos.y >= (chunkPos.y+1)*C.chunkH)return false;
    return true;
  }
  private function set_connections(node:BaseNode){
    if(!on_chunk(node.pos))return;
    var tx:Int = Std.int(node.pos.x).mod(8);
		var ty:Int = Std.int(node.pos.y).mod(8);
    var con:Array<Int> = node.connections;
    for(i in 0...con.length){
      var rot:Int = i;
      if(con[i] == 1){
        if(      con[(i+1)%4] == 1 && con[(i+2)%4] == 0 && con[(i+3)%4] == 0){
          tile_at("connections", tx, ty).id = C.tiles["connector1"];
        }else if(con[(i+1)%4] == 1 && con[(i+2)%4] == 1 && con[(i+3)%4] == 0){
          tile_at("connections", tx, ty).id = C.tiles["connector3"];

        }else if(con[(i+1)%4] == 0 && con[(i+2)%4] == 1 && con[(i+3)%4] == 0){
          tile_at("connections", tx, ty).id = C.tiles["connector2"];

        }else if(con[(i+1)%4] == 1 && con[(i+2)%4] == 1 && con[(i+3)%4] == 1){
          tile_at("connections", tx, ty).id = C.tiles["connector4"];

        }else if(con[(i+1)%4] == 0 && con[(i+2)%4] == 0 && con[(i+3)%4] == 0){
          tile_at("connections", tx, ty).id = C.tiles["connector0"];

        }else if(con[(i+1)%4] == 0 && con[(i+2)%4] == 0 && con[(i+3)%4] == 1){
          tile_at("connections", tx, ty).id = C.tiles["connector1"];
          rot = -1;
        }else if(con[(i+1)%4] == 0 && con[(i+2)%4] == 1 && con[(i+3)%4] == 1){
          tile_at("connections", tx, ty).id = C.tiles["connector3"];
          rot = -2;
        }else if(con[(i+1)%4] == 1 && con[(i+2)%4] == 0 && con[(i+3)%4] == 1){
          tile_at("connections", tx, ty).id = C.tiles["connector3"];
          rot = -1;
        }
        tile_at("connections", tx, ty).angle = 90 * rot;
        break;
      }
    }
  }
}
