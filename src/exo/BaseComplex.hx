package exo;
import luxe.Vector;
import luxe.Entity;
import luxe.Rectangle;
import luxe.tween.Actuate;
import exo.map.ChunkLoader;

enum NodeType{
	EMPTY;
	HUB;
	AIRLOCK;
	AIRLOCK_CLOSED;
	CRAFTING;
	REFINING;
	OXYGEN;
	TELEPORTER;
	FARM;
	ELECTRICITY;
	SOLAR;
}

typedef BaseNode = {
	var type:NodeType;
	var connections:Array<Int>;
	var pos:Vector;
	var base:BaseComplex;
}

class BaseComplex{
	public var nodes:Array<BaseNode>;
	public var bounds:Rectangle;
	private var center:Entity;


	public function new(p:Vector){
		nodes = new Array<BaseNode>();
		nodes.push({type: NodeType.HUB, connections: new Array<Int>(), pos: p, base: this});
		bounds = new Rectangle(0,0);
		center = new Entity({});
	}

	public function can_build_at(v:Vector, ?t:NodeType){
		var surr:Array<NodeType> = [
			get_node_at(new Vector(v.x,v.y-1))==null ? null : get_node_at(new Vector(v.x,v.y-1)).type,
			get_node_at(new Vector(v.x+1,v.y))==null ? null : get_node_at(new Vector(v.x+1,v.y)).type,
			get_node_at(new Vector(v.x,v.y+1))==null ? null : get_node_at(new Vector(v.x,v.y+1)).type,
			get_node_at(new Vector(v.x-1,v.y))==null ? null : get_node_at(new Vector(v.x-1,v.y)).type
		];

		switch(t){
			case NodeType.SOLAR:
				if(surr[0] == NodeType.ELECTRICITY ||
					 surr[1] == NodeType.ELECTRICITY ||
					 surr[2] == NodeType.ELECTRICITY ||
					 surr[3] == NodeType.ELECTRICITY)return true;
			default:
				if(surr[0] != null || surr[1] != null || surr[2] != null || surr[3] != null)return true;
		}
		return false;
	}

	public function addNode(p:Vector, t:NodeType){
		var node:BaseNode = {type: t, connections: new Array<Int>(), pos: p, base: this};
		nodes.push(node);
		//TODO:30 Recalculate only for new node
		recalculate_connections();
		recalculate_bounds();
		ChunkLoader.set_node_at(p, node);
	}

	public function placeNode(pos:Vector, t:NodeType){
		var idx:Int = Std.int(pos.x + pos.y*C.chunkW);
		var canPlace:Bool = false;
		var x:Int = Std.int(pos.x);
		var y:Int = Std.int(pos.y);
		if(can_build_at(pos, t))addNode(pos, t);
	}

	public function updateNode(pos:Vector, t:NodeType){
		get_node_at(pos).type = t;
		ChunkLoader.set_node_at(pos, get_node_at(pos));
	}

	public function get_node_at(pos:Vector){
		for(node in nodes){
			if(node.pos.equals(pos))return node;
		}
		return null;
	}

	public function focus_camera(?border:Float=0){
		var scaleX:Float = Luxe.screen.width/(bounds.w+C.mapScale*16*border);
		var scaleY:Float = Luxe.screen.height/(bounds.h+C.mapScale*16*border);
		var scale:Float = Math.min(Math.min(scaleX, scaleY), 6);
		//TODO:40 Get a reliable fix for pixel perfect scaling
		if(scale == 3.75)scale = 3.333333;
		Actuate.tween(Luxe.camera, .4, {zoom: scale}).ease(luxe.tween.easing.Quad.easeIn);
		Luxe.camera.focus(center.pos, .095);
	}

	private function recalculate_connections(){
		for(node in nodes){
			var x:Int = Std.int(node.pos.x);
			var y:Int = Std.int(node.pos.y);
			if(get_node_at(new Vector(x,y-1)) != null)node.connections[0] = 1;
			if(get_node_at(new Vector(x+1,y)) != null)node.connections[1] = 1;
			if(get_node_at(new Vector(x,y+1)) != null)node.connections[2] = 1;
			if(get_node_at(new Vector(x-1,y)) != null)node.connections[3] = 1;
		}
	}
	private function recalculate_bounds(){
		var max:Vector = nodes[0].pos.clone();
		var min:Vector = nodes[0].pos.clone();
		for(node in nodes){
			var nx:Int = Std.int(node.pos.x);
			var ny:Int = Std.int(node.pos.y);
			if(nx < min.x)min.x = nx;
			if(ny < min.y)min.y = ny;
			if(nx > max.x)max.x = nx;
			if(ny > max.y)max.y = ny;
		}
		max.addScalar(1);
		max.multiplyScalar(16*C.mapScale);
		min.multiplyScalar(16*C.mapScale);
		bounds = new Rectangle(min.x, min.y, max.x-min.x,max.y-min.y);
		center.pos = new Vector(bounds.x + bounds.w/2, bounds.y + bounds.h/2);
	}
}
