package exo;

import luxe.Sprite;
import phoenix.Texture;
import luxe.Vector;
import luxe.Color;
import luxe.components.sprite.SpriteAnimation;

enum Activity{
	WALKING;
	BUILDING;
	CRAFTING;
}

class Player extends Sprite{
	public var oxytankSize:Int = 25;
	public var mineralsBlue:Int = 0;
	public var mineralsRed:Int = 0;
	public var oxygen:Int;
	public var gridPos:Vector = new Vector(5,5);
	public var activity:Activity = Activity.WALKING;
	public var canMove:Bool = true;
	public var anim:SpriteAnimation;
	private var view_cone:Sprite;
	override public function new(){
		var t:Texture = Luxe.resources.texture("res/player.png");
		t.filter_min = t.filter_mag = FilterType.nearest;
		super({
			name: "character",
			texture: t,
			size: new Vector(48,48),
			pos: new Vector(0,0),
			depth: 1,
		});

		oxygen = oxytankSize;
		anim = new SpriteAnimation({name: "anim"});
		add(anim);
		anim.add_from_json_object(Luxe.resources.json("res/json/player_animation.json").asset.json);
		anim.animation = "down_idle";
		anim.play();
	}

	public function update_position(){
		pos = Vector.Multiply(gridPos, 16*C.mapScale).add(new Vector(8,8).multiplyScalar(C.mapScale));
	}

	override public function update(dt:Float){
		super.update(dt);
	}
}
