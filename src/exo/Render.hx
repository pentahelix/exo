package exo;

import phoenix.Renderer;
import phoenix.RenderTexture;
import phoenix.Texture.ClampType;
import phoenix.Batcher.BatcherEventType;
import phoenix.Batcher;
import luxe.Sprite;
import luxe.Vector;

class Render{
  public static var renderTex:RenderTexture;
  public static var ui:UI;
  public static function setup():RenderTexture{
    var tex:RenderTexture = new RenderTexture({
			id: "renderTex",
			clamp_t: ClampType.repeat,
			clamp_s: ClampType.repeat,
		});
		tex.slot = 0;
    var batch = new Batcher(Luxe.renderer, "batch");
    batch.view = new phoenix.Camera();
    batch.layer = 1;

    Luxe.renderer.batcher.on(BatcherEventType.prerender, prerender_batch);
    Luxe.renderer.batcher.on(BatcherEventType.postrender, postrender_batch);

    var view:Sprite = new Sprite({
        centered : false,
        pos : new Vector(0,0),
        size : Luxe.screen.size,
        texture : tex,
        batcher: batch,
        shader: Luxe.resources.shader("screen"),
    });

    Luxe.renderer.add_batch(batch);
    ui = new UI(tex);
    Luxe.renderer.add_batch(ui);
    renderTex = tex;
    return tex;
  }

  public static function prerender_batch(batch:Batcher){
    Luxe.renderer.target = renderTex;
  }

  public static function postrender_batch(batch:Batcher){
    Luxe.renderer.target = null;
  }

}
