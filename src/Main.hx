package;

import luxe.Screen.WindowEvent;
import luxe.Vector;
import luxe.Color;
import luxe.States;
import luxe.Input;
import luxe.Events;
import exo.Render;

class Main extends luxe.Game {
	var initialState: String = 'menu';
	var showCursor: Bool = true;

	public static var state:States;

	override function ready() {
		Render.setup();
		bind_input();
		state = new States({ name: "states" });
		state.add (new exo.states.MenuState({name: 'menu'}));
		state.add (new exo.states.WorldState({name: 'world'}));
		state.set("menu");
	}


	// CONFIG
	private function bind_input(){
		Luxe.input.bind_key('move_up',    Key.key_w);
		Luxe.input.bind_key('move_down',  Key.key_s);
		Luxe.input.bind_key('move_left',  Key.key_a);
		Luxe.input.bind_key('move_right', Key.key_d);
		Luxe.input.bind_key('interact',   Key.key_e);
	}

	override function config(config:luxe.AppConfig):luxe.AppConfig {
		config.preload.textures = [
			{id:'res/tiles.png'},
			{id:'res/player.png'},
			{id:'res/mineral_blue.png'},
			{id:'res/mineral_red.png'},
			{id:'res/oxygen.png'},
			{id:'res/base_cursor.png'},
			{id:'res/intro_planetTex.png'},
			{id:'res/intro_ship.png'},
			{id:'res/title.png'},
			{id:'res/nebula0.png'},
			{id:'res/nebula1.png'},
		];
		config.preload.shaders = [
			{id:'blurBelow', frag_id:'res/shaders/blurBelow.glsl', vert_id: 'default'},
			{id:'tint', frag_id:'res/shaders/tint.glsl', vert_id: 'default'},
			{id:'sphere', frag_id:'res/shaders/sphere.glsl', vert_id: 'default'},
			{id:'scroll', frag_id:'res/shaders/scroll.glsl', vert_id: 'default'},
			{id:'screen', frag_id:'res/shaders/screen.glsl', vert_id: 'default'},
		];
		config.preload.fonts = [
			{id: 'res/font/font.fnt'}
		];

		config.preload.jsons = [
			{id: 'res/json/player_animation.json'},
		];

		return config;
	}
}
