#version 120

uniform sampler2D tex0;
uniform vec2 uShift;
uniform vec4 transform;

varying vec2 tcoord;
varying vec4 color;

const int gaussRadius = 11;
const float gaussFilter[gaussRadius] = float[gaussRadius](
	0.0402,0.0623,0.0877,0.1120,0.1297,0.1362,0.1297,0.1120,0.0877,0.0623,0.0402
);

void main() {
	vec2 coords = vec2(tcoord.x, tcoord.y);
	coords.x *= transform.z;
	coords.y *= transform.w;
	coords += transform.xy;
	coords.y = 1 - coords.y;
	vec2 texCoord = coords - float(int(gaussRadius/2)) * uShift;
	vec2 offset = vec2(texCoord);
	vec3 c = vec3(0.0, 0.0, 0.0);
	for (int y = 0; y < gaussRadius; y++) {
		for (int x = 0; x < gaussRadius; x++) {
			c += gaussFilter[x] * gaussFilter[y] * texture2D(tex0, offset).xyz;
			offset.x += uShift.x;
		}
		offset.x = texCoord.x;
		offset.y += uShift.y;
	}
	gl_FragColor = vec4(c,1.0)*.84+color*.16;
}
