#version 120
uniform sampler2D tex0;
uniform float offset;

varying vec2 tcoord;
varying vec4 color;

void main(){
  vec2 p = -1.0 + 2.0 * tcoord;
  p = vec2(floor(p.x/(1./64.))*(1./64.)*1.1, floor(p.y/(1./64.))*(1./64.)*1.1);
  float r = sqrt(dot(p,p));
  if (r < 1.0){
    vec2 uv;
    float f = (1.0-sqrt(1.0-r))/(r);
    uv.x = p.x*f + offset;
    uv.y = p.y*f;
    gl_FragColor = texture2D(tex0,uv) * color;
  }else{
    if(1-pow(r,2)+.08 > 0){
      gl_FragColor = vec4(.5,.5,1,.2);
    }else{
      gl_FragColor = vec4(0,0,0,0);
    }
  }
}
