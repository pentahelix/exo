#version 120
uniform sampler2D tex0;
uniform float offset;

varying vec2 tcoord;
varying vec4 color;

void main(){
  vec2 uv = tcoord + vec2(offset,0);
  gl_FragColor = texture2D(tex0,uv) * color;
}
