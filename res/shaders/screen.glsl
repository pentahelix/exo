#version 120
uniform sampler2D tex0;
uniform int mode = 0;
uniform vec4 orange =     vec4(223.0/255.0, 113.0/255.0, 38.0/255.0, 1);
uniform vec4 white =      vec4(1, 1, 1, 1);
uniform vec4 grey_light = vec4(155.0/255.0,173.0/255.0,183.0/255.0,1);
uniform vec4 grey_dark =  vec4(89.0/255.0, 86.0/255.0, 82.0/255.0,1);
uniform vec4 blue =       vec4(0,107.0/255.0,183.0/255.0,1);

varying vec2 tcoord;
varying vec4 color;

void main(){
  switch(mode){
    case 0:
      gl_FragColor = texture2D(tex0, tcoord);
    break;
    case 1:
      if(texture2D(tex0, tcoord) == orange || texture2D(tex0, tcoord) == white || texture2D(tex0, tcoord) == grey_light || texture2D(tex0, tcoord) == grey_dark){
        gl_FragColor = white;
        return;
      }
      gl_FragColor = blue;
      return;
  }
}
